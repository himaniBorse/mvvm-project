package com.mobile_data_usage.api
import com.google.gson.annotations.SerializedName

data class RepoSearchResponse (
    val help: String,
    val success: Boolean,
    val result: Result,
    val error: Error
)

data class Result (
    @SerializedName("resource_id")
    val resourceID: String,
    val fields: List<Field>,
    val records: List<Record>,
    @SerializedName( "_links")
    val links: Links,
    val total: Long
)

data class Field (
    val type: String,
    val id: String
)

data class Links (
    val start: String,
    val prev: String,
    val next: String
)

data class Record (
    @SerializedName("volume_of_mobile_data")
    val volumeOfMobileData: String,
    val quarter: String,
    @SerializedName("_id")
    val id: Long
)