package com.mobile_data_usage.api

import retrofit2.http.GET
import retrofit2.http.Query

interface MobileService {
    @GET("action/datastore_search")
    suspend fun searchRepos(
        @Query("resource_id") resourceId: String,
        @Query("limit") limit: Int
    ): RepoSearchResponse
}
