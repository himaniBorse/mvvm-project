package com.mobile_data_usage.data

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.filter
import com.mobile_data_usage.api.MobileService
import com.mobile_data_usage.api.Record
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.mapLatest
import javax.inject.Inject

class MobileServiceRepository @Inject  constructor(private val service: MobileService) {

    fun getSearchResultStream(query: String): Flow<PagingData<Record>> {
        return Pager(
            config = PagingConfig(pageSize = NETWORK_PAGE_SIZE, enablePlaceholders = false),
            pagingSourceFactory = { MobileServiceNetworkPagingSource(service) }
        ).flow.mapLatest { list ->
            list.filter {
                it.quarter.contains(query)
            }
        }
    }

    companion object {
        const val NETWORK_PAGE_SIZE = 5
    }
}
