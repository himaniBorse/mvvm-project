package com.mobile_data_usage.data

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.mobile_data_usage.api.MobileService
import com.mobile_data_usage.api.Record
import com.mobile_data_usage.api.RepoSearchResponse
import java.io.IOException

class MobileServiceNetworkPagingSource(
    private val service: MobileService
) : PagingSource<Int,  Record>() {
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Record> {

        try {
            val response: RepoSearchResponse  = service.searchRepos(RESOURCE_ID, 5)
            return if(response.success){
                LoadResult.Page(
                    data = response.result.records,
                    prevKey = null,
                    nextKey = null
                )
            }else{
                LoadResult.Error(response.error)
            }
            } catch (exception: IOException) {
             return LoadResult.Error(exception)
         }
    }

    override fun getRefreshKey(state: PagingState<Int, Record>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }

    private val RESOURCE_ID = "a807b7ab-6cad-4aa6-87d0-e283a7353a0f"
}
