package com.mobile_data_usage.ui

import com.mobile_data_usage.api.Record

interface ClickListener {
    fun onclick(record: Record)
}