package com.mobile_data_usage.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.mobile_data_usage.api.Record
import com.mobile_data_usage.myapplication.R
import com.mobile_data_usage.myapplication.databinding.RecyclerviewItemRowBinding

class RepositoryPageAdapter(private val listener: ClickListener) :
    PagingDataAdapter<Record, RepositoryPageAdapter.RepositoryViewHolder>(RepoComparator) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepositoryViewHolder {
        val layoutView = LayoutInflater.from(parent.context)
        val bindingView = RecyclerviewItemRowBinding.inflate(layoutView, parent, false)
        return RepositoryViewHolder(bindingView, parent.context)
    }

    override fun onBindViewHolder(holder: RepositoryViewHolder, position: Int) {
        holder.bind(getItem(position), listener)
    }

    class RepositoryViewHolder(private val itemRowBinding: RecyclerviewItemRowBinding, private val context: Context) :
        RecyclerView.ViewHolder(itemRowBinding.root) {

        fun bind(repositoryViewData: Record?, listener: ClickListener) {
            itemRowBinding.mobileDataVolumn.text = repositoryViewData?.volumeOfMobileData?.let {
                concatenateData(context.getString(R.string.volumn_of_mobile_data), it)
            }

            itemRowBinding.quarter.text = repositoryViewData?.quarter?.let {
                concatenateData(context.getString(R.string.quarter), it)
            }

            itemRowBinding.id.text = repositoryViewData?.id?.let {
                concatenateData(context.getString(R.string._id), it.toString())
            }
        itemRowBinding.mainLayout.setOnClickListener {
            if (repositoryViewData != null) {
                listener.onclick(record =repositoryViewData )
            }
        }


        }
        private fun concatenateData(string: String, apiResData: String): String{
            return  string.plus(" : ").plus(apiResData)
        }
    }


    object RepoComparator : DiffUtil.ItemCallback<Record>() {
        override fun areItemsTheSame(
            oldItem: Record,
            newItem: Record
        ): Boolean {
            return oldItem.quarter == newItem.quarter
        }

        override fun areContentsTheSame(
            oldItem: Record,
            newItem: Record
        ): Boolean {
            return oldItem == newItem
        }
    }
}