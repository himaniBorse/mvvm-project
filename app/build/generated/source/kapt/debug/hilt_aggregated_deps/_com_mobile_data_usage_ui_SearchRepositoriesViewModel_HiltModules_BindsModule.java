package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.ViewModelComponent",
    modules = "com.mobile_data_usage.ui.SearchRepositoriesViewModel_HiltModules.BindsModule"
)
public class _com_mobile_data_usage_ui_SearchRepositoriesViewModel_HiltModules_BindsModule {
}
