package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.components.SingletonComponent",
    entryPoints = "com.mobile_data_usage.MyApp_GeneratedInjector"
)
public class _com_mobile_data_usage_MyApp_GeneratedInjector {
}
