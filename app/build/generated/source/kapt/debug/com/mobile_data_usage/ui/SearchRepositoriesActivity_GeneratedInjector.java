package com.mobile_data_usage.ui;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = SearchRepositoriesActivity.class
)
@GeneratedEntryPoint
@InstallIn(ActivityComponent.class)
public interface SearchRepositoriesActivity_GeneratedInjector {
  void injectSearchRepositoriesActivity(SearchRepositoriesActivity searchRepositoriesActivity);
}
