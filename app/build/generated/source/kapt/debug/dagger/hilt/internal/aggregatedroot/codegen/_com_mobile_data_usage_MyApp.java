package dagger.hilt.internal.aggregatedroot.codegen;

import dagger.hilt.android.HiltAndroidApp;
import dagger.hilt.internal.aggregatedroot.AggregatedRoot;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedRoot(
    root = "com.mobile_data_usage.MyApp",
    originatingRoot = "com.mobile_data_usage.MyApp",
    rootAnnotation = HiltAndroidApp.class
)
public class _com_mobile_data_usage_MyApp {
}
