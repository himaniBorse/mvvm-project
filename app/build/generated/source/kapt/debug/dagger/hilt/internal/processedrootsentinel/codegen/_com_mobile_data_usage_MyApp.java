package dagger.hilt.internal.processedrootsentinel.codegen;

import dagger.hilt.internal.processedrootsentinel.ProcessedRootSentinel;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@ProcessedRootSentinel(
    roots = "com.mobile_data_usage.MyApp"
)
public class _com_mobile_data_usage_MyApp {
}
