package com.mobile_data_usage.api;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\n\u0010\b\u001a\u00060\tj\u0002`\n\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0007H\u00c6\u0003J\r\u0010\u0017\u001a\u00060\tj\u0002`\nH\u00c6\u0003J5\u0010\u0018\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\f\b\u0002\u0010\b\u001a\u00060\tj\u0002`\nH\u00c6\u0001J\u0013\u0010\u0019\u001a\u00020\u00052\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001b\u001a\u00020\u001cH\u00d6\u0001J\t\u0010\u001d\u001a\u00020\u0003H\u00d6\u0001R\u0015\u0010\b\u001a\u00060\tj\u0002`\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013\u00a8\u0006\u001e"}, d2 = {"Lcom/mobile_data_usage/api/RepoSearchResponse;", "", "help", "", "success", "", "result", "Lcom/mobile_data_usage/api/Result;", "error", "Ljava/lang/Error;", "Lkotlin/Error;", "(Ljava/lang/String;ZLcom/mobile_data_usage/api/Result;Ljava/lang/Error;)V", "getError", "()Ljava/lang/Error;", "getHelp", "()Ljava/lang/String;", "getResult", "()Lcom/mobile_data_usage/api/Result;", "getSuccess", "()Z", "component1", "component2", "component3", "component4", "copy", "equals", "other", "hashCode", "", "toString", "app_debug"})
public final class RepoSearchResponse {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String help = null;
    private final boolean success = false;
    @org.jetbrains.annotations.NotNull()
    private final com.mobile_data_usage.api.Result result = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.Error error = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.mobile_data_usage.api.RepoSearchResponse copy(@org.jetbrains.annotations.NotNull()
    java.lang.String help, boolean success, @org.jetbrains.annotations.NotNull()
    com.mobile_data_usage.api.Result result, @org.jetbrains.annotations.NotNull()
    java.lang.Error error) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public RepoSearchResponse(@org.jetbrains.annotations.NotNull()
    java.lang.String help, boolean success, @org.jetbrains.annotations.NotNull()
    com.mobile_data_usage.api.Result result, @org.jetbrains.annotations.NotNull()
    java.lang.Error error) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getHelp() {
        return null;
    }
    
    public final boolean component2() {
        return false;
    }
    
    public final boolean getSuccess() {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.mobile_data_usage.api.Result component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.mobile_data_usage.api.Result getResult() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.Error component4() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.Error getError() {
        return null;
    }
}