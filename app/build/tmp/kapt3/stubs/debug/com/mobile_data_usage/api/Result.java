package com.mobile_data_usage.api;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0011\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B9\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0005\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f\u00a2\u0006\u0002\u0010\rJ\t\u0010\u0017\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J\u000f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\b0\u0005H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\nH\u00c6\u0003J\t\u0010\u001b\u001a\u00020\fH\u00c6\u0003JG\u0010\u001c\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\u000e\b\u0002\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u00052\b\b\u0002\u0010\t\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\fH\u00c6\u0001J\u0013\u0010\u001d\u001a\u00020\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010 \u001a\u00020!H\u00d6\u0001J\t\u0010\"\u001a\u00020\u0003H\u00d6\u0001R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0016\u0010\t\u001a\u00020\n8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u000fR\u0016\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\u000b\u001a\u00020\f\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016\u00a8\u0006#"}, d2 = {"Lcom/mobile_data_usage/api/Result;", "", "resourceID", "", "fields", "", "Lcom/mobile_data_usage/api/Field;", "records", "Lcom/mobile_data_usage/api/Record;", "links", "Lcom/mobile_data_usage/api/Links;", "total", "", "(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/mobile_data_usage/api/Links;J)V", "getFields", "()Ljava/util/List;", "getLinks", "()Lcom/mobile_data_usage/api/Links;", "getRecords", "getResourceID", "()Ljava/lang/String;", "getTotal", "()J", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "", "other", "hashCode", "", "toString", "app_debug"})
public final class Result {
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "resource_id")
    private final java.lang.String resourceID = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.mobile_data_usage.api.Field> fields = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.mobile_data_usage.api.Record> records = null;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "_links")
    private final com.mobile_data_usage.api.Links links = null;
    private final long total = 0L;
    
    @org.jetbrains.annotations.NotNull()
    public final com.mobile_data_usage.api.Result copy(@org.jetbrains.annotations.NotNull()
    java.lang.String resourceID, @org.jetbrains.annotations.NotNull()
    java.util.List<com.mobile_data_usage.api.Field> fields, @org.jetbrains.annotations.NotNull()
    java.util.List<com.mobile_data_usage.api.Record> records, @org.jetbrains.annotations.NotNull()
    com.mobile_data_usage.api.Links links, long total) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public Result(@org.jetbrains.annotations.NotNull()
    java.lang.String resourceID, @org.jetbrains.annotations.NotNull()
    java.util.List<com.mobile_data_usage.api.Field> fields, @org.jetbrains.annotations.NotNull()
    java.util.List<com.mobile_data_usage.api.Record> records, @org.jetbrains.annotations.NotNull()
    com.mobile_data_usage.api.Links links, long total) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getResourceID() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.mobile_data_usage.api.Field> component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.mobile_data_usage.api.Field> getFields() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.mobile_data_usage.api.Record> component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.mobile_data_usage.api.Record> getRecords() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.mobile_data_usage.api.Links component4() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.mobile_data_usage.api.Links getLinks() {
        return null;
    }
    
    public final long component5() {
        return 0L;
    }
    
    public final long getTotal() {
        return 0L;
    }
}