package com.mobile_data_usage.ui;

import java.lang.System;

@dagger.hilt.android.lifecycle.HiltViewModel()
@kotlinx.coroutines.ExperimentalCoroutinesApi()
@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u0010\u0018\u001a\u00020\nH\u0014J\u001c\u0010\u0019\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000f0\u000e2\u0006\u0010\u001a\u001a\u00020\u001bH\u0002R\u001d\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u001d\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000f0\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00150\u0014\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017\u00a8\u0006\u001c"}, d2 = {"Lcom/mobile_data_usage/ui/SearchRepositoriesViewModel;", "Landroidx/lifecycle/ViewModel;", "repository", "Lcom/mobile_data_usage/data/MobileServiceRepository;", "savedStateHandle", "Landroidx/lifecycle/SavedStateHandle;", "(Lcom/mobile_data_usage/data/MobileServiceRepository;Landroidx/lifecycle/SavedStateHandle;)V", "accept", "Lkotlin/Function1;", "Lcom/mobile_data_usage/ui/UiAction;", "", "getAccept", "()Lkotlin/jvm/functions/Function1;", "pagingDataFlow", "Lkotlinx/coroutines/flow/Flow;", "Landroidx/paging/PagingData;", "Lcom/mobile_data_usage/api/Record;", "getPagingDataFlow", "()Lkotlinx/coroutines/flow/Flow;", "state", "Lkotlinx/coroutines/flow/StateFlow;", "Lcom/mobile_data_usage/ui/UiState;", "getState", "()Lkotlinx/coroutines/flow/StateFlow;", "onCleared", "searchRepo", "queryString", "", "app_debug"})
public final class SearchRepositoriesViewModel extends androidx.lifecycle.ViewModel {
    private final com.mobile_data_usage.data.MobileServiceRepository repository = null;
    private final androidx.lifecycle.SavedStateHandle savedStateHandle = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlinx.coroutines.flow.StateFlow<com.mobile_data_usage.ui.UiState> state = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlinx.coroutines.flow.Flow<androidx.paging.PagingData<com.mobile_data_usage.api.Record>> pagingDataFlow = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.jvm.functions.Function1<com.mobile_data_usage.ui.UiAction, kotlin.Unit> accept = null;
    
    @javax.inject.Inject()
    public SearchRepositoriesViewModel(@org.jetbrains.annotations.NotNull()
    com.mobile_data_usage.data.MobileServiceRepository repository, @org.jetbrains.annotations.NotNull()
    androidx.lifecycle.SavedStateHandle savedStateHandle) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.StateFlow<com.mobile_data_usage.ui.UiState> getState() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<androidx.paging.PagingData<com.mobile_data_usage.api.Record>> getPagingDataFlow() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlin.jvm.functions.Function1<com.mobile_data_usage.ui.UiAction, kotlin.Unit> getAccept() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCleared() {
    }
    
    private final kotlinx.coroutines.flow.Flow<androidx.paging.PagingData<com.mobile_data_usage.api.Record>> searchRepo(java.lang.String queryString) {
        return null;
    }
}