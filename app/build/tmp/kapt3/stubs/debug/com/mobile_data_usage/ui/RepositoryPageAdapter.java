package com.mobile_data_usage.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0002\u0010\u0011B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0018\u0010\f\u001a\u00020\u00032\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u000bH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"}, d2 = {"Lcom/mobile_data_usage/ui/RepositoryPageAdapter;", "Landroidx/paging/PagingDataAdapter;", "Lcom/mobile_data_usage/api/Record;", "Lcom/mobile_data_usage/ui/RepositoryPageAdapter$RepositoryViewHolder;", "listener", "Lcom/mobile_data_usage/ui/ClickListener;", "(Lcom/mobile_data_usage/ui/ClickListener;)V", "onBindViewHolder", "", "holder", "position", "", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "RepoComparator", "RepositoryViewHolder", "app_debug"})
public final class RepositoryPageAdapter extends androidx.paging.PagingDataAdapter<com.mobile_data_usage.api.Record, com.mobile_data_usage.ui.RepositoryPageAdapter.RepositoryViewHolder> {
    private final com.mobile_data_usage.ui.ClickListener listener = null;
    
    public RepositoryPageAdapter(@org.jetbrains.annotations.NotNull()
    com.mobile_data_usage.ui.ClickListener listener) {
        super(null, null, null);
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.mobile_data_usage.ui.RepositoryPageAdapter.RepositoryViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.mobile_data_usage.ui.RepositoryPageAdapter.RepositoryViewHolder holder, int position) {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\u000b\u001a\u00020\fJ\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u000eH\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"}, d2 = {"Lcom/mobile_data_usage/ui/RepositoryPageAdapter$RepositoryViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemRowBinding", "Lcom/mobile_data_usage/myapplication/databinding/RecyclerviewItemRowBinding;", "context", "Landroid/content/Context;", "(Lcom/mobile_data_usage/myapplication/databinding/RecyclerviewItemRowBinding;Landroid/content/Context;)V", "bind", "", "repositoryViewData", "Lcom/mobile_data_usage/api/Record;", "listener", "Lcom/mobile_data_usage/ui/ClickListener;", "concatenateData", "", "string", "apiResData", "app_debug"})
    public static final class RepositoryViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        private final com.mobile_data_usage.myapplication.databinding.RecyclerviewItemRowBinding itemRowBinding = null;
        private final android.content.Context context = null;
        
        public RepositoryViewHolder(@org.jetbrains.annotations.NotNull()
        com.mobile_data_usage.myapplication.databinding.RecyclerviewItemRowBinding itemRowBinding, @org.jetbrains.annotations.NotNull()
        android.content.Context context) {
            super(null);
        }
        
        public final void bind(@org.jetbrains.annotations.Nullable()
        com.mobile_data_usage.api.Record repositoryViewData, @org.jetbrains.annotations.NotNull()
        com.mobile_data_usage.ui.ClickListener listener) {
        }
        
        private final java.lang.String concatenateData(java.lang.String string, java.lang.String apiResData) {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u00c6\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0003J\u0018\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0002H\u0016J\u0018\u0010\b\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0002H\u0016\u00a8\u0006\t"}, d2 = {"Lcom/mobile_data_usage/ui/RepositoryPageAdapter$RepoComparator;", "Landroidx/recyclerview/widget/DiffUtil$ItemCallback;", "Lcom/mobile_data_usage/api/Record;", "()V", "areContentsTheSame", "", "oldItem", "newItem", "areItemsTheSame", "app_debug"})
    public static final class RepoComparator extends androidx.recyclerview.widget.DiffUtil.ItemCallback<com.mobile_data_usage.api.Record> {
        @org.jetbrains.annotations.NotNull()
        public static final com.mobile_data_usage.ui.RepositoryPageAdapter.RepoComparator INSTANCE = null;
        
        private RepoComparator() {
            super();
        }
        
        @java.lang.Override()
        public boolean areItemsTheSame(@org.jetbrains.annotations.NotNull()
        com.mobile_data_usage.api.Record oldItem, @org.jetbrains.annotations.NotNull()
        com.mobile_data_usage.api.Record newItem) {
            return false;
        }
        
        @java.lang.Override()
        public boolean areContentsTheSame(@org.jetbrains.annotations.NotNull()
        com.mobile_data_usage.api.Record oldItem, @org.jetbrains.annotations.NotNull()
        com.mobile_data_usage.api.Record newItem) {
            return false;
        }
    }
}