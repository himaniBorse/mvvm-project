package com.mobile_data_usage.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0014JJ\u0010\r\u001a\u00020\n*\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00130\u00122\u0012\u0010\u0014\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00170\u00160\u00152\u0012\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u00020\n0\u0019H\u0002J.\u0010\u001b\u001a\u00020\n*\u00020\u000e2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00130\u00122\u0012\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\n0\u0019H\u0002JB\u0010\u001e\u001a\u00020\n*\u00020\u000e2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00130\u00122\u0012\u0010\u0014\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00170\u00160\u00152\u0012\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020\n0\u0019H\u0002J \u0010!\u001a\u00020\n*\u00020\u000e2\u0012\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\n0\u0019H\u0002R\u001b\u0010\u0003\u001a\u00020\u00048BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\""}, d2 = {"Lcom/mobile_data_usage/ui/SearchRepositoriesActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "viewModel", "Lcom/mobile_data_usage/ui/SearchRepositoriesViewModel;", "getViewModel", "()Lcom/mobile_data_usage/ui/SearchRepositoriesViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "bindList", "Lcom/mobile_data_usage/myapplication/databinding/ActivitySearchRepositoriesBinding;", "repoAdapter", "Lcom/mobile_data_usage/ui/RepositoryPageAdapter;", "uiState", "Lkotlinx/coroutines/flow/StateFlow;", "Lcom/mobile_data_usage/ui/UiState;", "pagingData", "Lkotlinx/coroutines/flow/Flow;", "Landroidx/paging/PagingData;", "Lcom/mobile_data_usage/api/Record;", "onScrollChanged", "Lkotlin/Function1;", "Lcom/mobile_data_usage/ui/UiAction$Scroll;", "bindSearch", "onQueryChanged", "Lcom/mobile_data_usage/ui/UiAction$Search;", "bindState", "uiActions", "Lcom/mobile_data_usage/ui/UiAction;", "updateRepoListFromInput", "app_debug"})
@dagger.hilt.android.AndroidEntryPoint()
public final class SearchRepositoriesActivity extends androidx.appcompat.app.AppCompatActivity {
    private final kotlin.Lazy viewModel$delegate = null;
    
    public SearchRepositoriesActivity() {
        super();
    }
    
    private final com.mobile_data_usage.ui.SearchRepositoriesViewModel getViewModel() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void bindState(com.mobile_data_usage.myapplication.databinding.ActivitySearchRepositoriesBinding $this$bindState, kotlinx.coroutines.flow.StateFlow<com.mobile_data_usage.ui.UiState> uiState, kotlinx.coroutines.flow.Flow<androidx.paging.PagingData<com.mobile_data_usage.api.Record>> pagingData, kotlin.jvm.functions.Function1<? super com.mobile_data_usage.ui.UiAction, kotlin.Unit> uiActions) {
    }
    
    private final void bindSearch(com.mobile_data_usage.myapplication.databinding.ActivitySearchRepositoriesBinding $this$bindSearch, kotlinx.coroutines.flow.StateFlow<com.mobile_data_usage.ui.UiState> uiState, kotlin.jvm.functions.Function1<? super com.mobile_data_usage.ui.UiAction.Search, kotlin.Unit> onQueryChanged) {
    }
    
    private final void updateRepoListFromInput(com.mobile_data_usage.myapplication.databinding.ActivitySearchRepositoriesBinding $this$updateRepoListFromInput, kotlin.jvm.functions.Function1<? super com.mobile_data_usage.ui.UiAction.Search, kotlin.Unit> onQueryChanged) {
    }
    
    private final void bindList(com.mobile_data_usage.myapplication.databinding.ActivitySearchRepositoriesBinding $this$bindList, com.mobile_data_usage.ui.RepositoryPageAdapter repoAdapter, kotlinx.coroutines.flow.StateFlow<com.mobile_data_usage.ui.UiState> uiState, kotlinx.coroutines.flow.Flow<androidx.paging.PagingData<com.mobile_data_usage.api.Record>> pagingData, kotlin.jvm.functions.Function1<? super com.mobile_data_usage.ui.UiAction.Scroll, kotlin.Unit> onScrollChanged) {
    }
}