package com.mobile_data_usage.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0004"}, d2 = {"DEFAULT_QUERY", "", "LAST_QUERY_SCROLLED", "LAST_SEARCH_QUERY", "app_debug"})
public final class SearchRepositoriesViewModelKt {
    private static final java.lang.String LAST_QUERY_SCROLLED = "last_query_scrolled";
    private static final java.lang.String LAST_SEARCH_QUERY = "last_search_query";
    private static final java.lang.String DEFAULT_QUERY = "";
}